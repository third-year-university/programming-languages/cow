import enum


class CowException(Exception):
    ...


class Interpreter:
    _memory: list[int] = [0]
    _index = 0
    _register = None
    _command_index = 0
    _commands = []

    def MoO(self):
        self._memory[self._index] += 1

    def MOo(self):
        self._memory[self._index] -= 1

    def moO(self):
        if len(self._memory) <= self._index + 1:
            self._memory.append(0)
        self._index += 1

    def mOo(self):
        if self._index <= 0:
            raise CowException(f"Index is out of range. CMD: {self._command_index}")
            # pass
        self._index -= 1

    def OOM(self):
        print(self._memory[self._index])

    def oom(self):
        print("\nInput cell value:", end=" ")
        value = input()
        if not value.isdigit():
            raise CowException(f"The value is not integer. CMD: {self._command_index}")
        self._memory[self._index] = int(value)

    def Moo(self):
        if self._memory[self._index] == 0:
            print("\nInput cell value:", end=" ")
            value = input()
            if len(value) != 1:
                raise CowException(f"The value is not char.  CMD: {self._command_index}")
            self._memory[self._index] = ord(value)
        else:
            print(chr(self._memory[self._index]), end="")

    def OOO(self):
        self._memory[self._index] = 0

    def mOO(self):
        command_index = self._memory[self._index]
        if command_index == 3:
            raise CowException(f"It looks like infinite loop. CMD: {self._command_index}")
        if command_index > len(self._commands):
            raise CowException(f"The program is finished. CMD: {self._command_index}")
        self._commands[command_index]()

    def MMM(self):
        if self._register is None:
            self._register = self._memory[self._index]
        else:
            self._memory[self._index] = self._register
            self._register = None

    def MOO(self):
        if self._memory[self._index] == 0:
            includes = 0
            self._command_index += 2
            while self._commands[self._command_index] != "moo" and includes == 0:
                # print(f"MOO Includes {includes}")
                if self._commands[self._command_index] == "MOO":
                    includes += 1
                if self._commands[self._command_index] == "moo" and includes > 0:
                    includes -= 1
                self._command_index += 1

    def moo(self):
        includes = 0
        self._command_index -= 2
        while self._commands[self._command_index] != "MOO" and includes == 0:
            # print(f"moo Includes {includes}")
            if self._commands[self._command_index] == "noo":
                includes += 1
            if self._commands[self._command_index] == "MOO" and includes > 0:
                includes -= 1
            self._command_index -= 1
        self._command_index -= 1

    _commands = [moo, mOo, moO, mOO, Moo, MOo, MoO, MOO, OOO, MMM, OOM, oom]

    def __init__(self, cmds) -> None:
        self._commands = cmds


    def execute(self):
        while self._command_index < len(self._commands):
            self._do_command(self._commands[self._command_index])
            self._command_index += 1

    def _do_command(self, command):
        match command:
            case "MoO":
                self.MoO()
            case "MOo":
                self.MOo()
            case "moO":
                self.moO()
            case "mOo":
                self.mOo()
            case "OOM":
                self.OOM()
            case "oom":
                self.oom()
            case "Moo":
                self.Moo()
            case "OOO":
                self.OOO()
            case "mOO":
                self.mOO()
            case "MMM":
                self.MMM()
            case "MOO":
                self.MOO()
            case "moo":
                self.moo()
            case _:
                print("Unknown command")
