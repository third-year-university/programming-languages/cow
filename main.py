import cow


def read_from_file(file_name):
    f = open(file_name)
    data = f.read()
    data.replace('\n', ' ')
    data.replace('\t', ' ')
    cmds = data.split()
    interpreter = cow.Interpreter(cmds)
    interpreter.execute()

read_from_file('hello.cow')
